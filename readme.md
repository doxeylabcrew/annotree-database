Download MySQL database dump at:

* Latest (Aug 25, 2020) Bacterial database https://zenodo.org/record/3998875#.X0W6EdP0nOQ  Archaeal database https://zenodo.org/record/4001161#.X0W6wNP0nOQ

* Aug 25, 2020 (bacteria) https://zenodo.org/record/3998875#.X0W6EdP0nOQ
* Aug 25, 2020 (archaea) https://zenodo.org/record/4001161#.X0W6wNP0nOQ
* Mar 29, 2020 (bacteria): https://zenodo.org/record/3732466#.XoDT39P0nOQ
* Mar 29, 2020 (archaea): https://zenodo.org/record/3732484#.XoDUtdP0nOQ
* Feb 1, 2019: https://zenodo.org/record/2555457/files/annotree-db-backup-2019-02-01.sql.tar.gz?download=1
* October 22, 2018: https://zenodo.org/record/1468869#.W84W-xP0nOQgtdb_bacteria-db-backup-2018-10-22.sql.tar.gz?download=1
* October 18, 2017: https://zenodo.org/record/1034451/files/phydo_db_2018_10_18.sql.tar.gz?download=1


Change Log:

* February 1, 2019: Updated to GTDB (RS86). Also includes TigrFam annotations. Refer to https://zenodo.org/record/2555457#.XHS0C5P0nOQ for full documentation
* October 22, 2018: Updated to GTDB Bacteria taxonomy (r83). Includes KEGG and Pfam annotations, protein sequences.
* October 18, 2017: First version, includes Pfam v30.0 on the Hug et al. (2016) tree of life based on NCBI taxonomy.
